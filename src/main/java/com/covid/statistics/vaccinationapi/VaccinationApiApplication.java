package com.covid.statistics.vaccinationapi;

import com.covid.statistics.vaccinationapi.repository.VaccinationRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.FileNotFoundException;
import java.io.IOException;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
public class VaccinationApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(VaccinationApiApplication.class, args);

        try {
            context.getBean(VaccinationRepository.class).retrieveAndInsertCSVs();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
