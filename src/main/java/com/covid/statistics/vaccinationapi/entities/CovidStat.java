package com.covid.statistics.vaccinationapi.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "COVID_STAT")
public class CovidStat implements Serializable {

    private Long id;

    private PopulationRegionalUnit area;

    private int dailyDose1;

    private int dailyDose2;

    private int dayDiff;

    private int dayTotal;

    private Date referenceDate ;

    private int totalDistinctPersons;

    private int totalDose1;

    private int totalDose2;

    private int totalVaccinations;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "AREA_ID")
    public PopulationRegionalUnit getArea() {
        return area;
    }

    public void setArea(PopulationRegionalUnit area) {
        this.area = area;
    }

    @Column(name = "DAILY_DOSE_1")
    public int getDailyDose1() {
        return dailyDose1;
    }

    public void setDailyDose1(int dailyDose1) {
        this.dailyDose1 = dailyDose1;
    }

    @Column(name = "DAILY_DOSE_2")
    public int getDailyDose2() {
        return dailyDose2;
    }

    public void setDailyDose2(int dailyDose2) {
        this.dailyDose2 = dailyDose2;
    }

    @Column(name = "DAY_DIFF")
    public int getDayDiff() {
        return dayDiff;
    }

    public void setDayDiff(int dayDiff) {
        this.dayDiff = dayDiff;
    }

    @Column(name = "DAY_TOTAL")
    public int getDayTotal() {
        return dayTotal;
    }

    public void setDayTotal(int dayTotal) {
        this.dayTotal = dayTotal;
    }

    @Column(name = "REFERENCE_DATE")
    public Date getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(Date referenceDate) {
        this.referenceDate = referenceDate;
    }

    @Column(name = "TOTAL_DISTINCT_PERSONS")
    public int getTotalDistinctPersons() {
        return totalDistinctPersons;
    }

    public void setTotalDistinctPersons(int totalDistinctPersons) {
        this.totalDistinctPersons = totalDistinctPersons;
    }

    @Column(name = "TOTAL_DOSE_1")
    public int getTotalDose1() {
        return totalDose1;
    }

    public void setTotalDose1(int totalDose1) {
        this.totalDose1 = totalDose1;
    }

    @Column(name = "TOTAL_DOSE_2")
    public int getTotalDose2() {
        return totalDose2;
    }

    public void setTotalDose2(int totalDose2) {
        this.totalDose2 = totalDose2;
    }

    @Column(name = "TOTAL_VACCINATIONS")
    public int getTotalVaccinations() {
        return totalVaccinations;
    }

    public void setTotalVaccinations(int totalVaccinations) {
        this.totalVaccinations = totalVaccinations;
    }
}
