package com.covid.statistics.vaccinationapi.dtos;

import com.opencsv.bean.CsvBindByName;

import java.io.Serializable;

public class CovidStatDTO implements Serializable {

    @CsvBindByName(column = "areaid")
    private Long areaId;

    @CsvBindByName(column = "dailydose1")
    private int dailyDose1;

    @CsvBindByName(column = "dailydose2")
    private int dailyDose2;

    @CsvBindByName(column = "daydiff")
    private int dayDiff;

    @CsvBindByName(column = "daytotal")
    private int dayTotal;

    @CsvBindByName(column = "referencedate")
    private String referenceDate ;

    @CsvBindByName(column = "totaldistinctpersons")
    private int totalDistinctPersons;

    @CsvBindByName(column = "totaldose1")
    private int totalDose1;

    @CsvBindByName(column = "totaldose2")
    private int totalDose2;

    @CsvBindByName(column = "totalvaccinations")
    private int totalVaccinations;

    public CovidStatDTO() {
    }

    public CovidStatDTO(Long areaId, int dailyDose1, int dailyDose2, int dayDiff, int dayTotal, String referenceDate, int totalDistinctPersons, int totalDose1, int totalDose2, int totalVaccinations) {
        this.areaId = areaId;
        this.dailyDose1 = dailyDose1;
        this.dailyDose2 = dailyDose2;
        this.dayDiff = dayDiff;
        this.dayTotal = dayTotal;
        this.referenceDate = referenceDate;
        this.totalDistinctPersons = totalDistinctPersons;
        this.totalDose1 = totalDose1;
        this.totalDose2 = totalDose2;
        this.totalVaccinations = totalVaccinations;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public int getDailyDose1() {
        return dailyDose1;
    }

    public void setDailyDose1(int dailyDose1) {
        this.dailyDose1 = dailyDose1;
    }

    public int getDailyDose2() {
        return dailyDose2;
    }

    public void setDailyDose2(int dailyDose2) {
        this.dailyDose2 = dailyDose2;
    }

    public int getDayDiff() {
        return dayDiff;
    }

    public void setDayDiff(int dayDiff) {
        this.dayDiff = dayDiff;
    }

    public int getDayTotal() {
        return dayTotal;
    }

    public void setDayTotal(int dayTotal) {
        this.dayTotal = dayTotal;
    }

    public String getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(String referenceDate) {
        this.referenceDate = referenceDate;
    }

    public int getTotalDistinctPersons() {
        return totalDistinctPersons;
    }

    public void setTotalDistinctPersons(int totalDistinctPersons) {
        this.totalDistinctPersons = totalDistinctPersons;
    }

    public int getTotalDose1() {
        return totalDose1;
    }

    public void setTotalDose1(int totalDose1) {
        this.totalDose1 = totalDose1;
    }

    public int getTotalDose2() {
        return totalDose2;
    }

    public void setTotalDose2(int totalDose2) {
        this.totalDose2 = totalDose2;
    }

    public int getTotalVaccinations() {
        return totalVaccinations;
    }

    public void setTotalVaccinations(int totalVaccinations) {
        this.totalVaccinations = totalVaccinations;
    }

    @Override
    public String toString() {
        return "CovidStatsDTO{" +
                "areaId=" + areaId +
                ", dailyDose1=" + dailyDose1 +
                ", dailyDose2=" + dailyDose2 +
                ", dayDiff=" + dayDiff +
                ", dayTotal=" + dayTotal +
                ", referenceDate=" + referenceDate +
                ", totalDistinctPersons=" + totalDistinctPersons +
                ", totalDose1=" + totalDose1 +
                ", totalDose2=" + totalDose2 +
                ", totalVaccinations=" + totalVaccinations +
                '}';
    }
}
