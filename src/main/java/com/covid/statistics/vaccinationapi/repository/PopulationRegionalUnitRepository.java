package com.covid.statistics.vaccinationapi.repository;

import com.covid.statistics.vaccinationapi.entities.PopulationRegionalUnit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PopulationRegionalUnitRepository extends CrudRepository <PopulationRegionalUnit, Long> {

    @Query("SELECT P FROM PopulationRegionalUnit P WHERE P.regionalUnit = ?1")
    PopulationRegionalUnit findByName(String regionalUnit);
}
