## Covid Vaccination API

This Application is an Spring Boot API running in an embedded embedded tomcat which will return statistics about covid-19.

###### How it works

On init, it is connecting with a postgresql database, creating shema and tables, reading csv files with data and importing the data to the database. After that API can be found in URL `host:9090/api/`.

Working URLs (e.g. host = localhost):
- http://localhost:9090/api/region/population?regionName='regionalUnit' = Return the population of a given region 
- http://localhost:9090/api/region/dailyTotalVaccination?regionName='regionalUnit' = Return a list of total vaccinations per day, for a given region
- http://localhost:9090/api/vacPercentPerRegion = Return the percentage of the population that has been vaccinated per region, for all regions

###### Make it work 

1. Install Java 11
2. Install postgresql 13
3. Create user `covid` with password `covid`
3. Install Maven
4. Clone the project in some folder
5. cd vaccination-api 
6. mvn clean install
7. mvn spring-boot:run
