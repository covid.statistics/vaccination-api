package com.covid.statistics.vaccinationapi.dtos;

import com.opencsv.bean.CsvBindByName;

import java.io.Serializable;

public class PopulationRegionalUnitDTO implements Serializable {

    @CsvBindByName(column = "Id")
    private Long id;

    @CsvBindByName(column = "Regional Unit")
    private String regionalUnit;

    @CsvBindByName(column = "Population")
    private String population;

    public PopulationRegionalUnitDTO() {
    }

    public PopulationRegionalUnitDTO(Long id, String regionalUnit, String population) {
        this.id = id;
        this.regionalUnit = regionalUnit;
        this.population = population;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegionalUnit() {
        return regionalUnit;
    }

    public void setRegionalUnit(String regionalUnit) {
        this.regionalUnit = regionalUnit;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "PopulationRegionalUnitsDTO{" +
                "id=" + id +
                ", regionalUnit='" + regionalUnit + '\'' +
                ", population='" + population + '\'' +
                '}';
    }
}
