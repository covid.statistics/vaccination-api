package com.covid.statistics.vaccinationapi.repository;

import com.covid.statistics.vaccinationapi.controllers.VaccinationController;
import com.covid.statistics.vaccinationapi.dtos.CovidStatDTO;
import com.covid.statistics.vaccinationapi.dtos.PopulationRegionalUnitDTO;
import com.covid.statistics.vaccinationapi.entities.CovidStat;
import com.covid.statistics.vaccinationapi.entities.PopulationRegionalUnit;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VaccinationRepository{
    Logger log = LoggerFactory.getLogger(VaccinationRepository.class);

    @Autowired
    CovidStatRepository covidStatRepository;

    @Autowired
    PopulationRegionalUnitRepository populationRegionalUnitRepository;



    public void retrieveAndInsertCSVs() throws IOException {
        List<PopulationRegionalUnit> populationRegionalUnitList = retrievePopulationRegionalUnitsFromCSV();
        populationRegionalUnitRepository.saveAll(populationRegionalUnitList);

        List<CovidStat> covidStatList = retrieveCovidStatsFromCSV();
        covidStatRepository.saveAll(covidStatList);

    }

    private List<CovidStat> retrieveCovidStatsFromCSV() throws FileNotFoundException {
        Reader reader = new BufferedReader(new FileReader("src/main/resources/csv/covidStats.csv"));
        // create csv bean reader
        CsvToBean<CovidStatDTO> csvToBean = new CsvToBeanBuilder(reader)
                .withType(CovidStatDTO.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

        List<CovidStatDTO> covidStatDTOList = csvToBean.parse();

        List<CovidStat> covidStatList = new ArrayList<>();
        for (CovidStatDTO aCovidStatDTO : covidStatDTOList) {
            CovidStat covidStat = convertCovidStatsDTOToCovidStats(aCovidStatDTO);
            covidStatList.add(covidStat);
        }

        return covidStatList;
    }

    private List<PopulationRegionalUnit> retrievePopulationRegionalUnitsFromCSV() throws IOException {
        Reader reader = new BufferedReader(new FileReader("src/main/resources/csv/populationRegionalUnits.csv", StandardCharsets.UTF_8));
            // create csv bean reader
        CsvToBean<PopulationRegionalUnitDTO> csvToBean = new CsvToBeanBuilder(reader)
                .withType(PopulationRegionalUnitDTO.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

        List<PopulationRegionalUnitDTO> populationRegionalUnitDTOList = csvToBean.parse();

        List<PopulationRegionalUnit> populationRegionalUnitList = new ArrayList<>();
        for (PopulationRegionalUnitDTO aPopulationRegionalUnitDTO : populationRegionalUnitDTOList) {
            PopulationRegionalUnit populationRegionalUnit = convertPopulationRegionalUnitsDTOToPopulationRegionalUnits(aPopulationRegionalUnitDTO);
            populationRegionalUnitList.add(populationRegionalUnit);
        }

        return populationRegionalUnitList;
    }

    private CovidStat convertCovidStatsDTOToCovidStats(CovidStatDTO covidStatDTO) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            CovidStat covidStat = new CovidStat();
            covidStat.setDailyDose1(covidStatDTO.getDailyDose1());
            covidStat.setDailyDose2(covidStatDTO.getDailyDose2());
            covidStat.setDayDiff(covidStatDTO.getDayDiff());
            covidStat.setDayTotal(covidStatDTO.getDayTotal());
            covidStat.setReferenceDate(sdf.parse(covidStatDTO.getReferenceDate()));
            covidStat.setTotalDistinctPersons(covidStatDTO.getTotalDistinctPersons());
            covidStat.setTotalDose1(covidStatDTO.getTotalDose1());
            covidStat.setTotalDose2(covidStatDTO.getTotalDose2());
            covidStat.setTotalVaccinations(covidStatDTO.getTotalVaccinations());
            covidStat.setArea(populationRegionalUnitRepository.findById(covidStatDTO.getAreaId()).orElse(null));
            return covidStat;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private PopulationRegionalUnit convertPopulationRegionalUnitsDTOToPopulationRegionalUnits(PopulationRegionalUnitDTO populationRegionalUnitDTO) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        PopulationRegionalUnit populationRegionalUnit = new PopulationRegionalUnit();
        populationRegionalUnit.setId(populationRegionalUnitDTO.getId());
        populationRegionalUnit.setRegionalUnit(populationRegionalUnitDTO.getRegionalUnit());
        populationRegionalUnit.setPopulation(Long.parseLong(populationRegionalUnitDTO.getPopulation().trim().replaceAll("\\.", "")));
        return populationRegionalUnit;
    }

}
