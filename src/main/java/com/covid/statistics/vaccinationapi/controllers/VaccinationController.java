package com.covid.statistics.vaccinationapi.controllers;

import com.covid.statistics.vaccinationapi.entities.CovidStat;
import com.covid.statistics.vaccinationapi.entities.PopulationRegionalUnit;
import com.covid.statistics.vaccinationapi.repository.CovidStatRepository;
import com.covid.statistics.vaccinationapi.repository.PopulationRegionalUnitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class VaccinationController {
    private static final Logger log = LoggerFactory.getLogger(VaccinationController.class);

    @Autowired
    CovidStatRepository covidStatRepository;

    @Autowired
    PopulationRegionalUnitRepository populationRegionalUnitRepository;

    @GetMapping("/region/population")
    public ResponseEntity<String> getPopulationOfRegion(@RequestParam String regionName) {
        final String _mn = "getPopulationOfRegion: ";

        log.info(_mn + "called for regionName = " + regionName);
        PopulationRegionalUnit populationRegionalUnit = populationRegionalUnitRepository.findByName(regionName);
        if (populationRegionalUnit == null) {
            return new ResponseEntity<>("Region not found", HttpStatus.OK);
        }
        return new ResponseEntity<>(String.valueOf(populationRegionalUnit.getPopulation()), HttpStatus.OK);
    }

    @GetMapping("/region/dailyTotalVaccination")
    public ResponseEntity<List<String>> getDailyTotalVaccinationOfRegion(@RequestParam String regionName) {
        final String _mn = "getPopulationOfRegion: ";

        log.info(_mn + "called for regionName = " + regionName);
        List<CovidStat> covidStatList = covidStatRepository.findByName(regionName);

        List<String> response = new ArrayList<>();

        if (covidStatList == null) {
            response.add("Region not found");
        }
        else {

            for (CovidStat aCovidStat : covidStatList) {
                response.add("Date : " + aCovidStat.getReferenceDate() + " , total vaccinations = " + aCovidStat.getTotalVaccinations());
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/vacPercentPerRegion")
    public ResponseEntity<Map<String, String>> getVacPercentPerRegion() {
        final String _mn = "getPopulationOfRegion: ";

        log.info(_mn + "called");
        List<Object[]> vacPercentPerRegionList = covidStatRepository.getVacPercentPerRegion();

        Map<String, String> vacPercentPerRegionMap= new HashMap<>();

        for (Object[] vacPercentPerRegion : vacPercentPerRegionList) {
            vacPercentPerRegionMap.put((String) vacPercentPerRegion[0], vacPercentPerRegion[1].toString() + "%");
        }

        return new ResponseEntity<>(vacPercentPerRegionMap, HttpStatus.OK);
    }
}
