package com.covid.statistics.vaccinationapi.repository;

import com.covid.statistics.vaccinationapi.entities.CovidStat;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CovidStatRepository extends CrudRepository <CovidStat, Long> {

    @Query("SELECT CS FROM CovidStat CS JOIN PopulationRegionalUnit P on P.id = CS.area WHERE P.regionalUnit = ?1 ORDER BY CS.referenceDate")
    List<CovidStat> findByName(String regionalUnit);

    @Query(value = "select p.regional_unit, (cs.total_distinct_persons * 100)/p.population perc_vac \n" +
            "from covid_stat cs \n" +
            "join population_regional_unit p on p.id  = cs.area_id \n" +
            "where cs.id in\n" +
            "(select distinct on (cs2.area_id) cs2.id \n" +
            "from covid_stat cs2\n" +
            "order by cs2.area_id , cs2.reference_date desc)", nativeQuery = true)
    List<Object[]> getVacPercentPerRegion();

}
